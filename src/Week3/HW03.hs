module Week3.HW03 where

data Expression =
    Var String                   -- Variable
  | Val Int                      -- Integer literal
  | Op Expression Bop Expression -- Operation
  deriving (Show, Eq)

-- Binary (2-input) operators
data Bop =
    Plus
  | Minus
  | Times
  | Divide
  | Gt
  | Ge
  | Lt
  | Le
  | Eql
  deriving (Show, Eq)

data Statement =
    Assign   String     Expression
  | Incr     String
  | If       Expression Statement  Statement
  | While    Expression Statement
  | For      Statement  Expression Statement Statement
  | Sequence Statement  Statement
  | Skip
  deriving (Show, Eq)

type State = String -> Int

-- Exercise 1 -----------------------------------------

extend :: State -> String -> Int -> State
-- The following two definitions are identical
-- extend state name val = \n -> if n == name then val else state n
extend state name val n = if n == name then val else state n

empty :: State
empty _ = 0

-- Exercise 2 -----------------------------------------

evalE :: State -> Expression -> Int
evalE state (Var name) = state name
evalE state (Val n) = n
evalE state (Op exp1 bop exp2) = apply bop (evalE state exp1) (evalE state exp2) where
  apply Plus x y = x + y
  apply Minus x y = x - y
  apply Times x y = x * y
  apply Divide x y = x `div` y
  apply Gt x y = if x > y then 1 else 0
  apply Ge x y = if x >= y then 1 else 0
  apply Lt x y = if x < y then 1 else 0
  apply Le x y = if x <= y then 1 else 0
  apply Eql x y = if x == y then 1 else 0

-- Exercise 3 -----------------------------------------

data DietStatement = DAssign String Expression
                   | DIf Expression DietStatement DietStatement
                   | DWhile Expression DietStatement
                   | DSequence DietStatement DietStatement
                   | DSkip
                     deriving (Show, Eq)

desugar :: Statement -> DietStatement
desugar (Assign name expr) = DAssign name expr
desugar (Incr name) = DAssign name (Op (Var name) Plus (Val 1))
desugar (If bExpr s1 s2) = DIf bExpr (desugar s1) (desugar s2)
desugar (While bExpr s) = DWhile bExpr (desugar s)
desugar (For s1 bExpr s2 s3) = desugar (Sequence s1 (While bExpr (Sequence s3 s2)))
desugar (Sequence s1 s2) = DSequence (desugar s1) (desugar s2)
desugar Skip = DSkip

-- Exercise 4 -----------------------------------------

evalSimple :: State -> DietStatement -> State
evalSimple state (DAssign name expr) = extend state name $ evalE state expr
evalSimple state (DIf expr ds1 ds2) =
                    if 1 == evalE state expr
                    then evalSimple state ds1
                    else evalSimple state ds2
evalSimple state (DWhile expr ds) =
                    if 1 == evalE state expr
                    then evalSimple state (DSequence ds (DWhile expr ds))
                    else state
evalSimple state (DSequence ds1 ds2) = evalSimple (evalSimple state ds1) ds2
evalSimple state DSkip = state

run :: State -> Statement -> State
run state stmt = evalSimple state $ desugar stmt

-- Programs -------------------------------------------

slist :: [Statement] -> Statement
slist [] = Skip
slist l  = foldr1 Sequence l

{- Calculate the factorial of the input

   for (Out := 1; In > 0; In := In - 1) {
     Out := In * Out
   }
-}
factorial :: Statement
factorial = For (Assign "Out" (Val 1))
                (Op (Var "In") Gt (Val 0))
                (Assign "In" (Op (Var "In") Minus (Val 1)))
                (Assign "Out" (Op (Var "In") Times (Var "Out")))

mult :: Statement
mult = Assign "Out" (Op (Var "X") Times (Var "Y"))

-- let s  = run (extend (extend empty "X" 4) "Y" 3) mult

sumN :: Statement
sumN = While (Op (Var "In") Gt (Val 0))
             (Sequence
               (Assign "Out" (Op (Var "Out") Plus (Var "In")))
               (Assign "In" (Op (Var "In") Minus (Val 1))))

-- let s  = run (extend empty "In" 8) sumN

ge :: Statement
ge = If (Op (Var "X") Lt (Var "Y"))
        (Assign "Out" (Val (-1)))
        (Assign "Out" (Val 1))

-- let s  = run (extend (extend empty "X" 4) "Y" 3) ge

{- Calculate the floor of the square root of the input

   B := 0;
   while (A >= B * B) {
     B++
   };
   B := B - 1
-}
squareRoot :: Statement
squareRoot = slist [ Assign "B" (Val 0)
                   , While (Op (Var "A") Ge (Op (Var "B") Times (Var "B")))
                       (Incr "B")
                   , Assign "B" (Op (Var "B") Minus (Val 1))
                   ]

{- Calculate the nth Fibonacci number

   F0 := 1;
   F1 := 1;
   if (In == 0) {
     Out := F0
   } else {
     if (In == 1) {
       Out := F1
     } else {
       for (C := 2; C <= In; C++) {
         T  := F0 + F1;
         F0 := F1;
         F1 := T;
         Out := T
       }
     }
   }
-}
fibonacci :: Statement
fibonacci = slist [ Assign "F0" (Val 1)
                  , Assign "F1" (Val 1)
                  , If (Op (Var "In") Eql (Val 0))
                       (Assign "Out" (Var "F0"))
                       (If (Op (Var "In") Eql (Val 1))
                           (Assign "Out" (Var "F1"))
                           (For (Assign "C" (Val 2))
                                (Op (Var "C") Le (Var "In"))
                                (Incr "C")
                                (slist
                                 [ Assign "T" (Op (Var "F0") Plus (Var "F1"))
                                 , Assign "F0" (Var "F1")
                                 , Assign "F1" (Var "T")
                                 , Assign "Out" (Var "T")
                                 ])
                           )
                       )
                  ]
