-- CIS 194, Spring 2015
--
-- Test cases for HW 05

module Week5.HW05Tests where

import Week5.HW05
import Testing

import qualified Data.ByteString.Lazy as BS

-- Exercise 1 -----------------------------------------

-- ex1Tests :: [Test]
-- ex1Tests = [ testF2 "getSecret test" getSecret
--              [ ("Week5/data/dog.jpg", "Week5/data/dog-original.jpg", "Haskell is Great!")
--              ]
--            ]
