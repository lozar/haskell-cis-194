{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE OverloadedStrings, RecordWildCards #-}
module Week5.HW05 where

import Data.ByteString.Lazy (ByteString)
import Data.Map.Strict (Map)
import Data.List (sortBy)
import Data.Ord (comparing)
import System.Environment (getArgs)

import qualified Data.ByteString.Lazy as BS
import qualified Data.Map.Strict as Map

import Data.Bits (xor)

import Week5.Parser

-- Exercise 1 -----------------------------------------

getSecret :: FilePath -> FilePath -> IO ByteString
getSecret f1 f2 = do
  xs <- BS.readFile f1
  ys <- BS.readFile f2
  return $ BS.pack $ filter (/= 0) (BS.zipWith xor xs ys)


-- Exercise 2 -----------------------------------------

decrypt :: ByteString -> ByteString -> ByteString
decrypt secret cipherText = BS.pack $ BS.zipWith xor cipherText (extend secret) where
  extend = BS.pack . cycle . BS.unpack

decryptWithKey :: ByteString -> FilePath -> IO ()
decryptWithKey secret fName = do
  cipherText <- BS.readFile fName
  BS.writeFile (dropSuffix fName) (decrypt secret cipherText) where
    dropSuffix = init . reverse . dropWhile (/= '.') . reverse

-- Exercise 3 -----------------------------------------

parseFile :: FromJSON a => FilePath -> IO (Maybe a)
parseFile fName = do
  content <- BS.readFile fName
  return $ decode content

-- Exercise 4 -----------------------------------------

filterIdMatch :: [TId] -> [Transaction] -> [Transaction]
filterIdMatch ids = filter (\t -> tid t `elem` ids)

getBadTs :: FilePath -> FilePath -> IO (Maybe [Transaction])
getBadTs vf tf = do
  victims <- parseFile vf :: IO (Maybe [TId])
  txns <- parseFile tf :: IO (Maybe [Transaction])
  let bads = filterIdMatch <$> victims <*> txns
  return bads

-- Exercise 5 -----------------------------------------

getFlow :: [Transaction] -> Map String Integer
getFlow = foldr trackFromTo Map.empty where
  trackFromTo txn tMap = tSum (to txn) (amount txn ) $ tSum (from txn) (negate $ amount txn) tMap
  tSum = Map.insertWith (+)

-- Exercise 6 -----------------------------------------

getCriminal :: Map String Integer -> String
getCriminal m = fst $ Map.foldrWithKey mkTuple ("",0) m where
  mkTuple k v t = if v > snd t then (k,v) else t

-- Exercise 7 -----------------------------------------

undoTs :: Map String Integer -> [TId] -> [Transaction]
undoTs flow txns = mkTransactions payers payees txns [] where
  mkTransactions [] _ _ acc = acc
  mkTransactions _ [] _ acc = acc
  mkTransactions _ _ [] acc = acc
  mkTransactions xl@(x:_) yl@(y:_) (t:ts) acc =
    let xfer = min (snd x) (snd y) in
      mkTransactions (check xl xfer) (check yl xfer) ts (Transaction {from=fst x, to=fst y, amount=abs xfer, tid=t} : acc)
  payType = Map.partition (> 0) (Map.filter (/= 0) flow)
  payers = sortBy (flip (comparing snd)) (Map.toList $ fst payType)
  payees = sortBy (flip (comparing snd)) (Map.toList $ snd payType)
  check [] _ = []
  check (x:xs) total = if snd x - total == 0 then xs else x:xs

-- Exercise 8 -----------------------------------------

writeJSON :: ToJSON a => FilePath -> a -> IO ()
writeJSON fName content = BS.writeFile fName $ encode content

-- Exercise 9 -----------------------------------------

doEverything :: FilePath -> FilePath -> FilePath -> FilePath -> FilePath
             -> FilePath -> IO String
doEverything dog1 dog2 trans vict fids out = do
  key <- getSecret dog1 dog2
  decryptWithKey key vict
  mts <- getBadTs vict trans
  case mts of
    Nothing -> error "No Transactions"
    Just ts -> do
      mids <- parseFile fids
      case mids of
        Nothing  -> error "No ids"
        Just ids -> do
          let flow = getFlow ts
          writeJSON "src/week5/data/foo" flow --TODO remove after checking
          writeJSON out (undoTs flow ids)
          return (getCriminal flow)

main :: IO ()
main = do
  args <- getArgs
  crim <-
    case args of
      dog1:dog2:trans:vict:ids:out:_ ->
          doEverything dog1 dog2 trans vict ids out
      _ -> doEverything "dog-original.jpg"
                        "dog.jpg"
                        "transactions.json"
                        "victims.json"
                        "new-ids.json"
                        "new-transactions.json"
  putStrLn crim
