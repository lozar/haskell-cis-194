{-# OPTIONS_GHC -Wall #-}
module Week4.HW04 where

import Data.List (intersperse)

newtype Poly a = P [a]

-- Exercise 1 -----------------------------------------

x :: Num a => Poly a
x = P [0, 1]

-- Exercise 2 ----------------------------------------

instance (Num a, Eq a) => Eq (Poly a) where
  (==) (P []) (P []) = True
  (==) (P []) (P (y:ys)) = y == 0 && P [] == P ys
  (==) (P (x':xs)) (P []) = x' == 0 && P xs == P []
  (==) (P (x':xs)) (P (y:ys)) = x' == y && P xs == P ys

-- Exercise 3 -----------------------------------------

instance (Num a, Eq a, Show a) => Show (Poly a) where
  show = showPoly

showPoly :: (Num a, Eq a, Show a) => Poly a -> String
showPoly (P xs) = if null result then "0" else concat result where
  result = reverse $ intersperse " + " terms
  terms = filter (not.null) $ zipWith showTerm xs ([0..]::[Integer])
  showTerm c e
    | c == 0 = ""
    | e == 0 = show c
    | e == 1 = case c of
                 1 -> "x"
                 -1 -> "-x"
                 _ -> show c ++ "x"
    | otherwise = show c ++ "x^" ++ show e

-- Exercise 4 -----------------------------------------

combine :: Num a => (a -> a -> a) -> [a] -> [a] -> [a]
combine _ [] ys' = ys'
combine _ xs' [] = xs'
combine f (x':xs') (y':ys') = f x' y' : combine f xs' ys'

plus :: Num a => Poly a -> Poly a -> Poly a
plus (P xs) (P ys) = P $ combine (+) xs ys

-- Exercise 5 -----------------------------------------

times :: Num a => Poly a -> Poly a -> Poly a
times (P xs) (P ys) =
  foldl plus (P []) (map P $ shiftMult xs (iterate (0:) ys) [])

shiftMult :: Num a => [a] -> [[a]] -> [[a]] -> [[a]]
shiftMult [] _ acc = acc
shiftMult _ [] acc = acc
shiftMult (x':xs) (y:ys) acc = shiftMult xs ys (map (*x') y : acc)

-- Exercise 6 -----------------------------------------

instance Num a => Num (Poly a) where
    (+) = plus
    (*) = times
    negate (P xs) = P $ map negate xs
    fromInteger n = P [fromIntegral n]
    -- No meaningful definitions exist
    abs    = undefined
    signum = undefined

-- Exercise 7 -----------------------------------------

applyP :: Num a => Poly a -> a -> a
applyP (P xs) n = sum $ zipWith (\x' e -> x' * n ^ e) xs ([0..]::[Integer])

-- Exercise 8 -----------------------------------------

class Num a => Differentiable a where
  deriv  :: a -> a
  nderiv :: Int -> a -> a
  nderiv 0 f = f
  nderiv n f = nderiv (n - 1) (deriv f)

-- Exercise 9 -----------------------------------------

-- instance (Num a, Enum a) => Differentiable (Poly a) where
--   deriv (P xs) = P $ zipWith (*) (tail xs) [1..]

instance (Num a) => Differentiable (Poly a) where
  deriv (P xs) = P $ mult (tail xs) 1 [] where
    mult [] _ _ = []
    mult (y:ys) n acc = y*n : mult ys (n+1) acc
