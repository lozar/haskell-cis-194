module Main where

import Test.Framework
import Test.Framework.Providers.HUnit
import Test.HUnit
import Test.QuickCheck
import System.CPUTime

import qualified Data.ByteString as BS
import qualified Data.Map.Strict as Map
import qualified Data.Vector as V

import qualified Week5.HW05 as W5 hiding (main)
import qualified Week5.Parser as W5P
import qualified Week7.HW07 as W7 hiding (main)

-- We assume the function we are timing is an IO monad computation
timeIt :: (Fractional c) => (a -> IO b) -> a -> IO c
timeIt action arg =
 do startTime <- getCPUTime
    _ <- action arg
    finishTime <- getCPUTime
    return $ fromIntegral (finishTime - startTime) / 1000000000000

-- Version for use with evaluating regular non-monadic functions
timeIt' :: (Fractional c) => (a -> b) -> a -> IO c
timeIt' f = timeIt (\x -> f x `seq` return ())

-- ====================================================================
-- These are the actual unit tests. The things above are helpers or
-- hard-coded test data
--
testGetSecret :: Test.HUnit.Test
testGetSecret = TestCase (
  do secret <- W5.getSecret "src/Week5/data/dog.jpg" "src/Week5/data/dog-original.jpg"
     assertEqual "Found secret," (show "Haskell Is Great!") (show secret))

testGetBadTs :: Test.HUnit.Test
testGetBadTs = TestCase (
  do ts <- W5.getBadTs "src/Week5/data/victims0.json" "src/Week5/data/transactions.json"
     assertEqual "Found bad transaction" (W5P.to.head <$> ts) (Just "Genaro Goins"))

testGetFlow :: Test.HUnit.Test
testGetFlow = TestCase $ assertEqual "Should return 15"
  15 (let ts = [W5P.Transaction {W5P.from="HC", W5P.to="SPC", W5P.amount=10, W5P.tid="abc"},
                W5P.Transaction {W5P.from="X", W5P.to="SPC", W5P.amount=5, W5P.tid=""}] in
          Map.findWithDefault 0 "SPC" $ W5.getFlow ts)

testGetCriminal :: Test.HUnit.Test
testGetCriminal = TestCase $ assertEqual "Should return SPC"
  "SPC" (let ts = [W5P.Transaction {W5P.from="HC", W5P.to="SPC", W5P.amount=10, W5P.tid="abc"},
                   W5P.Transaction {W5P.from="X", W5P.to="SPC", W5P.amount=5, W5P.tid=""}] in
             W5.getCriminal $ W5.getFlow ts)

testUndoTs :: Test.HUnit.Test
testUndoTs = TestCase (
  do txns <- W5.parseFile "src/Week5/data/new-transactions.json"
     assertEqual "Undo transaction sum" (Just 13089) ((sum . map W5P.amount) <$> txns))

testLiftM :: Test.HUnit.Test
testLiftM = TestCase $ assertEqual "Should return (Just 6)"
  (Just 6) $ W7.liftM (+1) (Just 5)

testSwapV :: Test.HUnit.Test
testSwapV = TestCase $ assertEqual "Should return (= Just (V.fromList [3, 2, 1])"
  (Just $ V.fromList [3, 2, 1]) $ W7.swapV 0 2 (V.fromList [1, 2, 3])

testBadSwapV :: Test.HUnit.Test
testBadSwapV = TestCase $ assertEqual "Should return Nothing"
  Nothing $ W7.swapV 0 3 (V.fromList [1, 2, 3])

testMapM :: Test.HUnit.Test
testMapM = TestCase $ assertEqual "Should return (Just [1..10])"
  (Just [1..10]) $ mapM Just [1..10]

testGetElts :: Test.HUnit.Test
testGetElts = TestCase $ assertEqual "Should return (Just [1,3])"
  (Just [1, 3]) $ W7.getElts [1,3] (V.fromList [0..9])

tests :: [Test.Framework.Test]
tests = hUnitTestToTests $ TestList [
    TestLabel "testGetSecret" testGetSecret
  , TestLabel "testGetBadTs" testGetBadTs
  , TestLabel "testGetFlow" testGetFlow
  , TestLabel "testGetCriminal" testGetCriminal
  , TestLabel "testUndoTs" testUndoTs
  , TestLabel "testLiftM" testLiftM
  , TestLabel "testSwapV" testSwapV
  , TestLabel "testBadSwapV" testBadSwapV
  , TestLabel "testMapM" testMapM
  , TestLabel "testGetElts" testGetElts
  ]

main :: IO ()
main = defaultMain tests
